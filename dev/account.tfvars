environment = "dev"

common_tags = {
  "Project"     = "vilar-corp"
  "Environment" = "dev"
  "Owner"       = "Vilar"
}
