terragrunt = {
  # Endereço do repositório do módulo
  terraform {
    source = "git::ssh://git@gitlab.com/terragrunt-techtalk/terragrunt-example/infrastructure-modules.git//network/vpc?ref=0.1.1"

    #source = "/home/mvilar/projetos/terraform_techtalk/terragrunt_example/infrastructure-modules/network/vpc"
  }

  # Incluir as configurações do terraform.tfvars da raiz
  include = {
    path = "${find_in_parent_folders()}"
  }
}

vpc_cidr_block = "10.20.0.0/16"

subnet_cidr_blocks = ["10.20.1.0/24", "10.20.2.0/24"]
