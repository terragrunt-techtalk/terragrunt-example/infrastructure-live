terragrunt = {
  # Endereço do repositório do módulo
  terraform {
    source = "git::ssh://git@gitlab.com/terragrunt-techtalk/terragrunt-example/infrastructure-modules.git//applications/web-server?ref=0.1.1"
  }

  dependencies {
    paths = ["../vpc"]
  }

  # Incluir as configurações do terraform.tfvars da raiz
  include = {
    path = "${find_in_parent_folders()}"
  }
}

git_repo = "https://gitlab.com/vilar.rivendel/web-sample.git"

git_version = "master"
