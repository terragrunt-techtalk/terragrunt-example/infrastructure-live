environment = "stage"

common_tags = {
  "Project"     = "vilar-corp"
  "Environment" = "stage"
  "Owner"       = "Vilar"
}
