terragrunt = {
  # Cofiguração para armazenamento do State pelo Terragrunt
  remote_state {
    backend = "s3"

    config {
      encrypt = true
      bucket  = "vilar-temp-stage"
      key     = "${path_relative_to_include()}/terraform.tfstate"
      region  = "us-east-2"
    }
  }

  # Configurar a leitura de configurações intermediárias
  terraform {
    extra_arguments "-var-file" {
      commands = ["${get_terraform_commands_that_need_vars()}"]

      optional_var_files = [
        "${get_tfvars_dir()}/${find_in_parent_folders("account.tfvars", "ignore")}",
        "${get_tfvars_dir()}/${find_in_parent_folders("region.tfvars", "ignore")}",
      ]
    }
  }
}
